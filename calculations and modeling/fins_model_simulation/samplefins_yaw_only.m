N = 20;
X = zeros((2*N + 1), 4);
Y = zeros((2*N + 1), 3);

u_inf_scalar =  8.2859; % terminal velocity
rpy = [0,0,0]; % roll,pitch yaw (Euler angles) in rad

counter = 1;
for phi_1 = -N:0.5:N
                Y(counter, :) = calcMoments(u_inf_scalar,[phi_1 phi_1 phi_1 phi_1], rpy);
                X(counter, :) = [phi_1 phi_1 phi_1 phi_1];
                counter = counter + 1;
            end

writematrix(Y(:,3),'moments_yaw.csv');
writematrix(X,'angles_yaw.csv');

