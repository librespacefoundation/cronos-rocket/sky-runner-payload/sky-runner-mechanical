//LOAD CELL
#include "HX711.h"
#define calibration_factor 29150 //This value is obtained using the HX711_load_cell_calibration sketch
#define LOADCELL_DOUT_PIN  3
#define LOADCELL_SCK_PIN  2
HX711 scale;

// HALL EFFECT
int hall_pin = 4;
float hall_thresh = 50.0; // set number of hall trips for RPM reading (higher improves accuracy)

//millis() 
#define DELAY_INTERVAL_1 500
#define DELAY_INTERVAL_2  1
unsigned long lastExecutedMillis_1 = 0; // vairable to save the last executed time for load cell code
unsigned long lastExecutedMillis_2 = 0; // vairable to save the last executed time for hall effect code




void setup() {
  Serial.begin(115200);
  Serial.println("WIND TUNNEL TEST");
  Serial.println("Don't forget to check that the initial values of load cell are approximately zero.");
  pinMode(hall_pin, INPUT);
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0
  Serial.println("Readings:");
}

void loop() {

  unsigned long currentMillis = millis();
    /********************
     ***** LOAD CELL ****
     ********************/

  if (currentMillis - lastExecutedMillis_1 >= DELAY_INTERVAL_1) {
    lastExecutedMillis_1 = currentMillis; // save the last executed time
  //Serial.print("Thrust: ");
  Serial.print(453.59237*scale.get_units(), 0);  
  Serial.print(",");
  //Serial.println();
  }

    /********************
     **** HALL EFFECT ***
     ********************/
  if (currentMillis - lastExecutedMillis_2 >= DELAY_INTERVAL_2) {
    lastExecutedMillis_2 = currentMillis; // save the last executed time
    
 float hall_count = 1.0;
  float start = micros();
  bool on_state = false;
  // counting number of times the hall sensor is tripped
  // but without double counting during the same trip
  while(true){
    if (digitalRead(hall_pin)==0){
      if (on_state==false){
        on_state = true;
        hall_count+=1.0;
      }
    } else{
      on_state = false;
    }
    
    if (hall_count>=hall_thresh){
      break;
    }
  }
  // print information about Time and RPM
  float end_time = micros();
  float time_passed = ((end_time-start)/1000000.0);
  //Serial.print("Time Passed: ");
  //Serial.print(time_passed);
  //Serial.print("s ");
  float rpm_val = (hall_count/time_passed)*60.0;
  Serial.println(rpm_val);
  //Serial.println(",");

  }


}
  
