 function v_w = wind_turbulence_gen(tspan)
%% This function generates turbulent wind velocities.

% v_w - wind linear velocities vector = [Uw;Vw;Ww] (*in inertial frame*) 
% tspan - length of tspan in ode45 integration(number of time steps used in integration of the dynamics of the system) 
%-> to generate the appropriate amount of sample values

%% Fist we generate White noise to represent the "turbulence term", which is 
%a zero mean gaussian distribution with a standand deviation of sigma1
mu1 = 0; %mean
sigma1 = 0.1; %Standard deviation (sigma^2 = variance)
v_wind = sigma1*randn(tspan,1) + mu1;
% % to visualize uncomment
% figure(1);
% plot(v_wind)
% title("White noise turbulence {v_{wind}}");
% xlabel("Samples")
% ylabel("Sample Values")
% grid on
%% These are the terms that represent wind intensities(which were taken from
%meteorological data for the specific locatiokn we want the launch to take
%place). They were modeled as Gaussian distributions as well. It is assumed
%that the wind in the down(Ww) direction in wolrld coordinate system is
%negligeble. Thus only north(Uw) and east(Vw) directions have non-zero terms of
%wind velocities.

mu2 = 0.44704*7.5; %mean 
sigma2 = sqrt(0.3048*0.1); %Standard deviation (sigma^2 = variance)
mu3 = -0.3048*0.5; %mean 
sigma3 = sqrt(0.3048*0.1); %Standard deviation (sigma^2 = variance)
V_wi = sigma2*randn(tspan,1) + mu2;
U_wi = sigma3*randn(tspan,1) + mu3;

% % to visualize uncomment
% figure(2);
% plot(V_wi)
% title("Intensity along the Y axis in inertial frame coordinates V_{wi}");%some error included
% xlabel("Samples")
% ylabel("Sample Values")
% grid on 
% 
% figure(3);
% plot(U_wi)
% title("Intensity along the X axis in inertial frame coordinates U_{wi}");%some error included
% xlabel("Samples")
% ylabel("Sample Values")
% grid on 
%% Afterwards we use a low-pass filter to smooth the turbulence
V_w = (1+v_wind).*V_wi;
V_w_final = lowpass(V_w,0.079577472,tspan); %time constant 2s %increase the turbulence by increasing the frequency
U_w = (1+v_wind).*U_wi;
U_w_final = lowpass(U_w,0.079577472,tspan); %time constant 2s 
% figure
% plot(U_w_final);
% figure
% plot(V_w_final);
W_w_final = zeros(tspan,1);
v_w = [V_w_final, U_w_final, W_w_final];

%References
%http://www.sengpielaudio.com/calculator-timeconstant.htm  
%https://dspace.mit.edu/bitstream/handle/1721.1/32456/61751366-MIT.pdf;sequence=2  (Simulation and Control Design of a Gliding Autogyro for Precision Airdrop by Joshua F. Torgerson p.27-28)
%https://weatherspark.com/countries/m/US/NM/6#Figures-WindSpeed 
 end
 