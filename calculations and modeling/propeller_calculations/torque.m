function Q = torque(u,omega,d,B)
%% This function gives the torque of the propeller given the descent
%velocity - u ,the spin rate of the rotor - omega, and air density - d
%%

geometry = load('r_chord_twist.mat');
r = geometry.chordtwistpropeller.r;
c = geometry.chordtwistpropeller.chord;
t = geometry.chordtwistpropeller.twist;

Q_differential = zeros(length(r)-1,1);

for i = 2:length(r)    
[a, a_t ]= inducion_factor(u,omega,r(i),B,c(i),t(i));
Q_differential(i-1) = 4*pi*r^3*d*u*omega*(1-a)*a_t*(r(i) - r(i-1));
end
Q = sum(Q_differential);

end