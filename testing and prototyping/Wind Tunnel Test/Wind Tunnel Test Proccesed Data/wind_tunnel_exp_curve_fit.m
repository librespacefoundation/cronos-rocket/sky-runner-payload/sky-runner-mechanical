% Import a .csv file( Home -> Import Data -> Select the file you want to import)
% We import a table, where the first column is the timestamp
% VarName1 is the default name matlab gives to columns of the table, you can
% change it if you click on the table in the workspace

d = datetime(WindTunnelTest5ms.VarName1) - datetime(WindTunnelTest5ms.VarName1(1));
WindTunnelTest5ms = zeros(size(WindTunnelTest5ms));
WindTunnelTest5ms(:,1) = seconds(d); % we convert the timestamp into seconds
WindTunnelTest5ms(:,2) = WindTunnelTest5ms.VarName2;
WindTunnelTest5ms(:,3) = WindTunnelTest5ms.VarName3;


%% Curve fitting(exponential function)
x = WindTunnelTest5ms(:,1);
y = WindTunnelTest5ms(:,2);
f = @(b,x) b(1).*exp(b(2).*x)+b(3);                        % Objective Function
B = fminsearch(@(b) norm(y - f(b,x)), [-200; -1; 100]);    % Estimate Parameters
% If an error occurs add a couple of zeros to the last value above!
figure()
scatter(x,y,'bo');  
hold on
y_fit = f(B,x);
plot(x, f(B,x))

xlim([0,100])
ylim([0,1000])
%ylim([1200,1800])
xlabel('time(s)')
ylabel('m(gr)')
ylabel('Ω_r(rpm)')
text(27, 105, sprintf('m = %.1f\\cdote^{%.3f\\cdott}%+.1f', B))
legend("Εxperimental Data","Curve Fitted Data")
title("Velocity: 12 m/s")
%plot your raw data