function T = thrust(u,omega,d,B)
%% To simulate run sim_plots.m
%% This function gives the thrust of the propeller given the descent
% velocity - u ,the spin rate of the rotor - omega, and air density - d
% B - number of blades
%%
geometry = load('r_chord_twist.mat');
r = geometry.chordtwistpropeller.r;
c = geometry.chordtwistpropeller.chord;
t = geometry.chordtwistpropeller.twist;
T_differential = zeros(length(r)-1,1);
% for a specific U, omega
for i = 2:length(r)    
a = inducion_factor(u,omega,r(i),B,c(i),t(i));
T_differential(i-1) = 4*pi*r*d*u^2*(1-a)*a*(r(i) - r(i-1));
end
T = sum(T_differential);

end
