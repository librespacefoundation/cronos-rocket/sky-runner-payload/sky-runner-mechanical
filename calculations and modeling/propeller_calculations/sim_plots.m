%% Run this file to simulate the dynamic system.
% This function simulates the dynamic system of vertical descent deriving
% data from a BEM simulation of a rotor performance.
% The documentation for the equations used and the numerical analysis method are described here:
% https://gitlab.com/white-noise/sky-runner/-/wikis/Propeller-thrust-and-torque-calculation
%% This code does not converge given the data of our model. Some possible solutions for this problem:
% - Include R_hub(distance from the center of rotation till the blade) and R_root = a section of a rotor blade near the hub that does not
% produce lift, and generates drag only 
% - Add lift coefficients for big angles of attack and add a procedure of
% linear interpolation, to make the code converge 
% - Iff induction factors do not converge set them equal to a small
% resonable number 
% - Reasonable induction factors for autorotation are small, because there is no actuation(0.01-0.05)
%% Differential eqns of motion
clc;
clear;
% Model Parameters
cdf = 1.05; %drag coeff for cube (ref: https://en.wikipedia.org/wiki/Drag_coefficient)
Af = 0.1*0.1;  %10cmx10cm cubesat fuselage referance area 
B = 3; %number of blades 
g = 9.81;
d = 1.184;%dry air density,25° C,sea level
J = 1.96e5*1e-9; % Rotor moment of inertia (1g.mm2 = 1e-9 kg.m2)
R = 0.5;
m = 0.5;


%Initial Conditions
t = 10; %from 0 
dt = 0.1;   
u = zeros(1,t/dt+2);
omega = zeros(1,t/dt+2);
u(1) = 20;
omega(1) = 2000/9.5492965964254; %1rad/s = 9.5492965964254rpm
 
for  i = 1:(t/dt+1)
T = thrust(u(i),omega(i),R,d);
Q = torque(u(i),omega(i),R,d);
Dfuselage = 0.5*Af*cdf*d*u(i)^2;
u_dot = g - T/m - Dfuselage/m;
omega_dot = Q/J;
u(i + 1) = u(i) +  dt*u_dot;
omega(i + 1) = omega(i) +  dt*omega_dot;
end
t = 0:0.1:10.1;
figure(1);
plot(t,u)
xlabel('t')
ylabel('u')
figure(2);
plot(t,omega)
xlabel('t')
ylabel('Omega')


