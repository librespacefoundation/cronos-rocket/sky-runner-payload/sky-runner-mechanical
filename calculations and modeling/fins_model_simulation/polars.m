function [C_L,C_D,C_M] = polars(alpha)

%% Function Input
% alpha: angle of attack given in degrees

%% Variables Explanation
% Polars
% Column1: Angle of attack in deg
% Column2: C_L
% Column3: C_D
% Column4: C_M


%%%     fin characteristics             %%%
%%%     a   Cl      Cd          Cm      %%%
Polars = [    0	0	0.03951	0
    0.5	0.2145	0.03814	-0.0278
    1	0.3623	0.03591	-0.0436
    1.5	0.4855	0.03338	-0.0549
    2	0.5672	0.03158	-0.0596
    2.5	0.5951	0.03132	-0.0557
    3	0.6234	0.0313	-0.0517
    3.5	0.6537	0.03134	-0.0476
    4	0.6882	0.03126	-0.0437
    4.5	0.715	0.03196	-0.0394
    5	0.7445	0.03263	-0.035
    5.5	0.7781	0.03311	-0.0307
    6	0.7997	0.03451	-0.0255
    6.5	0.8361	0.03515	-0.0214
    7	0.8505	0.03715	-0.0152
    7.5	0.876	0.03861	-0.0101
    8	0.9088	0.04006	-0.0059
    8.5	0.9154	0.04285	0.001
    9	0.9228	0.04583	0.0075
    9.5	0.9299	0.04904	0.0137
    10	0.9423	0.05231	0.0189
    10.5	0.8127	0.06405	0.0316
    90	0	1.2	-0.8];
% This was imported for fast fins sampling because it is slow to access data from txt everytime

    if alpha < 0 
        alpha = -alpha;
    end
        %Inserting polars from a txt file generated by xfoil 
        %Xfoil_data = readmatrix('NACA0018.txt');
        %Polars = Xfoil_data(:, [1:3,5]);
        C_L = interp1(Polars(:,1),Polars(:,2),abs(alpha));
        C_D = interp1(Polars(:,1),Polars(:,3),abs(alpha));
        C_M = interp1(Polars(:,1),Polars(:,4),abs(alpha));
end


