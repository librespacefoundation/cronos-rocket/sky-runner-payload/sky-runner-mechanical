vel =[4,5,6,7,8,9,10,11,12];
rpm = [657.578,977.073,1190.94,1385.15,1578.64,1549.69,1732.54,1888.21,2064.57];
loadcell = [147.409,236.354,316.747,362.632,488.186,672.773,852.198,1008.44,1262.87];

x = vel;
y = rpm;
n = 8;
%Use polyfit to fit a nth-degree polynomial to the points.
p = polyfit(x,y,n);
%Evaluate the polynomial on a finer grid and plot the results.
x1 = vel;
y1 = polyval(p,x1);
figure
plot(x,y,'o')
hold on
plot(x1,y1)
xlabel('u(m/s)')
ylabel('m(gr)')
%ylabel('Ω_r(rpm)')
legend("Εxperimental Data","Curve Fitted Data")
title('8st degree polynomial fit')
%title('1st degree polynomial fit')
%caption = sprintf('Ω = %.2fu + %.2f', p(1), p(2));
%text(8, 800, caption, 'FontSize', 15);

caption = sprintf('Ω = %.2fu^8%.2fu^7+%.2fu^6%.2fu^5+%.2fu^4%.2fu^3+%.2fu^2%.2fu+%.2f', p(1), p(2), p(3),p(4),p(5),p(6),p(7),p(8), p(9));
text(4.5, 800, caption, 'FontSize', 15);

hold off


%% Resultant formulas for quick computations
mass = 538;%in gr
syms vel %m/s
eqn_mass = mass == 0.00924422122334169*vel^8-0.627613789262610*vel^7+18.4491961694438*vel^6-305.840910946291*vel^5+3117.39268062050*vel^4-19939.2616296677*vel^3 + 77907.0733477960*vel^2-169482.375279184*vel	+156914.134952027;
vel_eval = double(solve(eqn_mass)); 
vel = 8.2859;
omega = 0.193727281630147*vel^8-12.2776124928464*vel^7+334.583444254674*vel^6-5116.93816800990*vel^5+47999.0311457803*vel^4-282602.975469421*vel^3+1019206.64814144*vel^2-2057073.30059171*vel+1778234.05515580;

