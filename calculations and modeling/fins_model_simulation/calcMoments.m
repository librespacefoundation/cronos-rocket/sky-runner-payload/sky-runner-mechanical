function [TMOMENT,TLIFT,TDRAG] = calcMoments(u_inf_scalar,fin_angles, rpy)

% A function that calculates total 3D moment and forces applied on the body of the
% autogyro 

%Input:
% u_inf: Descent velocity -> will be calculated from a Kalman Filter using altimeter values
% rpy: [Roll,pitch yaw] (Euler angles) in rad -> will be calculated from a Complementary IMU filter, using a 9DOF IMU
% fin_angle: [a1,a2,a3,a4] The angle of the fins


%Parameters:
rho = 1.225;    %air density
chord = 0.059;
span = 0.027;     
S = span*chord; % fin area

% The fins are arranged in a circullar pattern
Rmin = 0.102/2;
Rmax = 0.156/2;
fin_dist = 1/2*(Rmax+Rmin);  % distance from the 1/2 of the point span to the center of the fin assembly
z = 0.1;    % vertical distance of 1/4 of the chord from CG
% Refer to the documentation to understand why these particular values are used


pitch = rpy(1);
roll = rpy(2);
yaw = rpy(3);

RotationMatrix = ([cos(yaw) -sin(yaw) 0; sin(yaw) cos(yaw) 0; 0 0 1]*[cos(roll) 0 sin(roll); 0 1 0; -sin(roll) 0 cos(roll)]*[1 0 0; 0 cos(pitch) -sin(pitch); 0 sin(pitch) cos(pitch)])';
u_inf = RotationMatrix*[0;0;u_inf_scalar]; %free stream velocity vector


% Frame vectors for fins
n1 = [0;1;0];
n2 = [-1;0;0];
n3 = [0;-1;0];
n4 = [1;0;0];
n = [n1,n2,n3,n4];


L = zeros(4,3);
D = zeros(4,3);
M = zeros(4,3);


% Calculate polars
[C_L,C_D,C_M] = polars(fin_angles);
% Loop over each fin
 for i = 1:4
     % Calculate lift 
     e_L = cross(u_inf,n(:,i))./norm(cross(n(:,i),u_inf));
     L(i,:) =  sign(fin_angles(i))*0.5*C_L(i)*rho*S*norm(u_inf)^2*e_L;
     % Calculate drag
     e_D = u_inf/norm(u_inf);
     D(i,:) = 0.5*C_D(i)*rho*S*norm(u_inf)^2*e_D;
     % Calculate moment
     r = n(:,i)*fin_dist+ [0;0;z]; % distance from the CG vector
     Moment_lift = cross(r,L(i,:));
     Moment_drag = cross(r,D(i,:));
     Moment_airfoil =  sign(fin_angles(i))*0.5*C_M(i)*rho*S*chord*norm(u_inf)^2*n(:,i)';
     M(i,:) = Moment_lift + Moment_drag + Moment_airfoil;     
 end
 
 

     TLIFT  = sum(L);
     TDRAG = sum(D);
     TMOMENT = sum(M);
      
end
